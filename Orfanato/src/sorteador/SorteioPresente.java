package sorteador;

import java.util.Random;

public class SorteioPresente {

	public static void main(String[] args) {
		String[] criancas = { 
				"Carlos M 2", 
				"Larisa F 5", 
				"Suzana F 10", 
				"Miguel M 8", 
				"Luceli F 7", 
				"Joaldo M 5",
				"Marisa F 11",
				"Joanes F 2",
				"Arieso M 7",
				"Jonata M 10",
		};
		
		String[][] presentes = { 
				{ "Maquiagem", "F", "11" }, 
				{ "Bola", "M", "3" }, 
				{ "Avi�o", "M", "5" },
				{ "Boneca", "F", "3" }, 
				{ "Flauta", "F", "7" }, 
				{ "Lego", "M", "10" },
				{ "Bombol�", "F", "4" },
				{ "Pega-Vareta", "M", "6" },
				{ "Pistola-d'�gua", "M", "11" },
				{ "Kit Princesa", "F", "5" },
		};
		
		for (int i = 0; i < criancas.length; i++) {
			switch (criancas[i].substring(criancas.length-3,criancas.length-2)) {
			case "F":
				escolheIdadeFeminino(presentes,criancas, i);
				break;
			case "M":
				escolheIdadeMasculino(presentes,criancas, i);
				break;

			default:
				break;
			}
		}


	}
	
	public static int aleatorio() {
		Random random = new Random();
		int num = random.nextInt(11);
				
		return num;
	}
	
	public static void imprimir(String txt) {
		System.out.println(txt+" ");
	}
	
	public static void escolheIdadeMasculino(String[][] presentes,String[] criancas,int i) {
		int x= aleatorio();
		switch (criancas[i].substring(criancas.length-1,criancas.length)) {
		case "1": case "2": case "3": 
			while (((!presentes[x][2].equals("1"))||(!presentes[x][2].equals("2"))||(!presentes[x][2].equals("3")))&& (!presentes[x][1].equals("M"))) {
				x=aleatorio();
			}
			imprimir("Crian�a: "+criancas[i].substring(0,3)+" Presente: "+presentes[x][0]);
			break;
		
		case "4": case "5": case "6":
			while (((!presentes[x][2].equals("4"))||(!presentes[x][2].equals("5"))||(!presentes[x][2].equals("6")))&& (!presentes[x][1].equals("M"))) {
				x=aleatorio();
			}
			imprimir("Crian�a: "+criancas[i].substring(0,3)+" Presente: "+presentes[x][0]);
			break;
		case "7": case "8": case "9": case "10":
			while (((!presentes[x][2].equals("7"))||(!presentes[x][2].equals("8"))||(!presentes[x][2].equals("9"))||(!presentes[x][2].equals("10")))&& (!presentes[x][1].equals("M"))) {
				x=aleatorio();
			}
			imprimir("Crian�a: "+criancas[i].substring(0,3)+" Presente: "+presentes[x][0]);
			break;

		case "11":
			while (((!presentes[x][2].equals("11")))&& (!presentes[x][1].equals("M"))) {
				x=aleatorio();
			}
			imprimir("Crian�a: "+criancas[i].substring(0,3)+" Presente: "+presentes[x][0]);	
			break;
		default:
			System.out.println("Erro");
			break;
			}
		}
		public static void escolheIdadeFeminino(String[][] presentes,String[] criancas,int i) {
			int x= aleatorio();
			switch (criancas[i].substring(criancas.length-1,criancas.length)) {
			case "1": case "2": case "3": 
				while (((!presentes[x][2].equals("1"))||(!presentes[x][2].equals("2"))||(!presentes[x][2].equals("3")))&& (!presentes[x][1].equals("F"))) {
					x=aleatorio();
				}
				imprimir("Crian�a: "+criancas[i].substring(0,3)+" Presente: "+presentes[x][0]);
				break;
				
			case "4": case "5": case "6":
				while (((!presentes[x][2].equals("4"))||(!presentes[x][2].equals("5"))||(!presentes[x][2].equals("6")))&& (!presentes[x][1].equals("F"))) {
					x=aleatorio();
				}
				imprimir("Crian�a: "+criancas[i].substring(0,3)+" Presente: "+presentes[x][0]);
				break;
			case "7": case "8": case "9": case "10":
				while (((!presentes[x][2].equals("7"))||(!presentes[x][2].equals("8"))||(!presentes[x][2].equals("9"))||(!presentes[x][2].equals("10")))&& (!presentes[x][1].equals("F"))) {
					x=aleatorio();
				}
				imprimir("Crian�a: "+criancas[i].substring(0,3)+" Presente: "+presentes[x][0]);
				break;
				
			case "11":
				while (((!presentes[x][2].equals("11")))&& (!presentes[x][1].equals("F"))) {
					x=aleatorio();
				}
				imprimir("Crian�a: "+criancas[i].substring(0,3)+" Presente: "+presentes[x][0]);
				break;
			default:
				System.out.println("Erro");
				break;
			}
	}

}
